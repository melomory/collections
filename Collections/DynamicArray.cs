﻿using System;
using System.Collections;
using System.Collections.Generic;
using Collections.Interfaces;

namespace Collections
{
	public class DynamicArray<T> : IDynamicArray<T>
	{
		private T[] _arr;

		public DynamicArray()
		{
			Capacity = 8;
			Length = 0;
			Array.Resize(ref _arr, Capacity);
		}

		public DynamicArray(int capacity)
		{
			if (capacity < 0)
            {
				throw new ArgumentException("Capacity is less than 0", nameof(capacity));
            }

			Capacity = capacity;
			Length = 0;
			Array.Resize(ref _arr, Capacity);
		}

		public DynamicArray(IEnumerable<T> items)
		{
			int count = 0;
            foreach (var item in items)
            {
				count++;
            }

			Capacity = count;
			Length = 0;
			Array.Resize(ref _arr, Capacity);

			foreach (var item in items)
            {
				_arr[Length++] = item;
            }
		}

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < Length; i++)
            {
				yield return _arr[i];
            }
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public T this[int index] 
		{
			get
            {
                if (index < 0 || index >= Length)
                {
                    throw new IndexOutOfRangeException();
                }

                return _arr[index];
			}
			
			set
            {
				_arr[index] = value;
			}
		}

		public int Length { get; private set; }

		public int Capacity { get; private set; }

		public void Add(T item)
		{
			if (Length == Capacity)
            {
				if (Capacity == 0)
                {
					Capacity++;
				}

				Capacity *= 2;
				Array.Resize(ref _arr, Capacity);
			}

			Length++;
			_arr[Length - 1] = item;			
		}

		public void AddRange(IEnumerable<T> items)
		{
			int count = 0;
			foreach (var item in items)
			{
                count++;
			}

			int expectedNumberOfItems = count + Length;
			if (expectedNumberOfItems > Capacity)
            {
                while (expectedNumberOfItems > Capacity)
                {
                    Capacity *= 2;
                }
                Array.Resize(ref _arr, Capacity);
            }

			foreach (var item in items)
			{
				_arr[Length++] = item;
			}
		}

		public void Insert(T item, int index)
		{
			if (Length == Capacity)
            {
				Capacity *= 2;
				Array.Resize(ref _arr, Capacity);
			}

			T savedItem = _arr[index];
			_arr[index] = item;

			index++;
			for (int i = Length; i > index; i--)
            {
				_arr[i] = _arr[i - 1];
            }
			_arr[index] = savedItem;

			Length++;
		}

		public bool Remove(T item)
		{
			int index = Array.IndexOf(_arr, item);

			if (index >= 0)
            {
				index++;
				for (int i = index; i < Length; i++)
                {
					_arr[index - 1] = _arr[index];
                }

				Length--;

				return true;
            }

			return false;
		}
	}
}